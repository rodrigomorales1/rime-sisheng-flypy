# Rime schema
# encoding: utf-8

# TOOD: Insert the syllable "è"
# TOOD: Insert the syllable "ó"
# TODO: Insert the syllable "shai" as in "晒" (shài)
# TODO: Insert the syllable "ǹg" as in "嗯"
# FIX: Insert the syllable "lüè" as in "忽略" (hūlüè)
#      NOTE: Press "lt" for typing "lüè". Recall that there are no
#      syllables of the form "lue", so that string shouldn't be
#      inserted.
# FIX: Insert the syllable "lüè" as in "虐待" (nüèdài)
#      NOTE: Press "nt" for typing "nüè". Recall that there are no
#      syllables of the form "nue", so that string shouldn't be
#      inserted.
# DONE: Insert the syllable "ang" as in "昂贵" (ángguì)
#       ANSWER: Press "ah" for typing "ang".
# TODO: Insert the syllable "hng" as in "哼" (hng)
# TODO: Pressing SPC without pressing a number should insert the
#       syllable without a tone. My finger struggles to reach the number 5.
# TODO: Add syllables êếêề as in "欸"
# TODO: Add syllable "hm" as in "噷"
# TODO: Add syllable "hng" as in "哼"

schema:
  schema_id: sisheng_flypy
  name: 音節 - 小鶴雙拼
  version: “0.1”
  author:
    - double pinyin layout by 鶴
    - Rime schema by 佛振 <chen.sst@gmail.com>
    - 饞貓渣
    - just4u1314
    - 吳善
  description: |
    Insert pinyin syllables with tones. The dictionary and the schema
    of this input method was retrieved from
    https://github.com/wu-shan/type-pinyin and were later modified.

switches:
  - name: ascii_mode
    reset: 0
    states: [ 中文, 西文 ]
  - name: full_shape
    states: [ 半角, 全角 ]
  - name: ascii_punct
    states: [ ，。, ，． ]
  - name: simplification
    states: [ 漢字, 汉字 ]

engine:
  processors:
    - ascii_composer
    - recognizer
    - key_binder
    - speller
    - punctuator
    - selector
    - navigator
    - express_editor
  segmentors:
    - ascii_segmentor
    - matcher
    - abc_segmentor
    - punct_segmentor
    - fallback_segmentor
  translators:
    - punct_translator
    - reverse_lookup_translator
    - script_translator
  filters:
    - simplifier
    - uniquifier

speller:
  alphabet: zyxwvutsrqponmlkjihgfedcbaABCDEFGHIJKLMNOPQRSTUVWXYZ
  delimiter: " '"
  algebra:
    - erase/^xx$/
    - derive/^([jqxy])u$/$1v/
    - derive/^([aoe])([ioun])$/$1$1$2/
    - xform/^([aoe])(ng)?$/$1$1$2/
    - xform/iu$/q/
    - xform/(.)ei$/$1w/
    - xform/uan$/r/
    - xform/[uv]e$/t/
    - xform/un$/y/
    - xform/^Sh/U/
    - xform/^sh/u/
    - xform/^ch/i/
    - xform/^Ch/I/
    - xform/^Zh/V/
    - xform/^zh/v/
    - xform/uo$/o/
    - xform/(.)ie$/$1p/
    - xform/([xXqQjJ])iong$/$1s/
    - xform/([iIvVzZyYtTsSrRnNlLkKhHgGdDcC])ong$/$1s/
    - xform/ing$|uai$/k/
    - xform/(.)ai$/$1d/
    - xform/(.)en$/$1f/
    - xform/(.)eng$/$1g/
    # This rule needs to be before the rule for "$1h". Otherwise, it
    # will have no effect.
    # jl -> jiang (讲)
    # ql -> qiang (强)
    # xl -> xiang (想)
    # nl -> niang (娘)
    # ll -> liang (两)
    - xform/([jJqQxXnNlL])iang$/$1l/
    # ul -> shuang
    # il -> chuang
    # vl -> zhuang
    # kl -> kuang (况)
    # hl -> huang (黄)
    # gl -> guang (广)
    - xform/([uUiIvVkKhHgG])uang$/$1l/
    - xform/(.)ang$/$1h/
    # This rule needs to be before the regular expression that matches
    # "(.)an". Otherwise, syllable that use "ian" as the final won't
    # be able to be inserted.
    - xform/(.)ian$/$1m/
    - xform/(.)an$/$1j/
    - xform/(.)ou$/$1z/
    # xx -> xia
    # qx -> qia
    # lx -> lia
    # jx -> jia
    # dx -> dia
    - xform/([xXqQlLjJdD])ia$/$1x/
    # ix -> chua
    # ux -> shua
    # vx -> zhua
    # gx -> gua
    # hx -> hua
    # kx -> kua
    - xform/([rRiIuUvVgGhHkK])ua$/$1x/
    # nn -> niao
    # bn -> biao
    - xform/(.)iao$/$1n/
    - xform/(.)ao$/$1c/
    - xform/(.)ui$/$1v/
    - xform/in$/b/

translator:
  dictionary: sisheng_flypy
  prism: pinyin_dpfly
  enable_user_dict: false
  # Define the rules for the text that is shown right above the
  # completion list and before a selection has been done
  preedit_format:
    - xform/([bpmfdtnljqx])n/$1iao/
    - xform/(\w)g/$1eng/
    - xform/(\w)q/$1iu/
    - xform/(\w)w/$1ei/
    - xform/([dtnlgkhjqxyvuirzcs])r/$1uan/
    - xform/(\w)t/$1ve/
    - xform/(\w)y/$1un/
    - xform/([dtnlgkhvuirzcs])o/$1uo/
    - xform/(\w)p/$1ie/
    - xform/([jqx])s/$1iong/
    - xform/(\w)s/$1ong/
    - xform/(\w)d/$1ai/
    - xform/(\w)f/$1en/
    - xform/(\w)h/$1ang/
    - xform/(\w)j/$1an/
    - xform/([gkhvuirzcs])k/$1uai/
    - xform/(\w)k/$1ing/
    - xform/(\w)z/$1ou/
    - xform/([gkhvuirzcs])x/$1ua/
    - xform/(\w)x/$1ia/
    - xform/(\w)c/$1ao/
    - xform/([dtgkhvuirzcs])v/$1ui/
    - xform/(\w)b/$1in/
    - xform/(\w)m/$1ian/
    - xform/([aoe])\1(\w)/$1$2/
    - "xform/(^|[ '])v/$1zh/"
    - "xform/(^|[ '])i/$1ch/"
    - "xform/(^|[ '])u/$1sh/"
    - xform/([jqxy])v/$1u/
    - xform/([nl])v/$1ü/
    - xform/([jqxnl])l/$1iang/
    - xform/([vuikhg])l/$1uang/

punctuator:
  import_preset: default

key_binder:
  import_preset: default
